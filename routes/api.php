<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Create client
Route::post('client', 'ClientController@store');

// Charge the account
Route::post('charge', 'PaymentController@charge');

// Transfer the money
Route::post('transfer', 'PaymentController@transfer');

// Set a quote
Route::post('quote', 'CurrencyController@store');