<?php

namespace App\Http\Controllers;

use App\Client;
use App\Currency;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class PaymentController extends Controller
{
    /**
     * Charge the account.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function charge(Request $request)
    {
        $client_id = $request->post('client');
        $currency = $request->post('currency');
        $value = $request->post('value');

        $client = Client::find($client_id);

        if (empty($client)) {
            return response()->json(['error' => 'Client not found.'], 400);
        }

        if (!Currency::check($currency)) {
            return response()->json(['error' => 'Currency is incorrect.'], 400);
        }

        if (empty($value)) {
            return response()->json(['error' => 'Value is not by null.'], 400);
        }

        $base_value = Currency::convertToBase($currency, $value);

        if ($currency == $client->currency) {
            $client_value = $value;
        } else {
            $client_value = Currency::convertFromBase($client->currency, $base_value);
        }

        $client = Payment::charge($client, $currency, $value, $base_value, $client_value);

        return response()->json($client, 200);
    }

    /**
     * Transfer the money.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function transfer(Request $request)
    {
        $sender = $request->post('sender');
        $recipient = $request->post('recipient');
        $currency = $request->post('currency');
        $value = $request->post('value');

        $client_sender = Client::find($sender);

        if (empty($client_sender)) {
            return response()->json(['error' => 'Sender not found.'], 400);
        }

        $client_recipient = Client::find($recipient);

        if (empty($client_recipient)) {
            return response()->json(['error' => 'Recipient not found.'], 400);
        }

        if (!Currency::check($currency)) {
            return response()->json(['error' => 'Currency is incorrect.'], 400);
        }

        if (empty($value)) {
            return response()->json(['error' => 'Value is not by null.'], 400);
        }

        $base_value = Currency::convertToBase($currency, $value);

        if ($client_sender->currency == $currency) {
            $client_sender_value = $value;
        } else {
            $client_sender_value = Currency::convertFromBase($client_sender->currency, $base_value);
        }

        if ($client_sender_value > $client_sender->balance) {
            return response()->json(['error' => 'Not enough funds to money transfer.'], 400);
        }

        if ($client_recipient->currency == $currency) {
            $client_recipient_value = $value;
        } else {
            $client_recipient_value = Currency::convertFromBase($client_recipient->currency, $base_value);
        }

        Payment::transfer($client_sender, $client_recipient, $currency, $value, $base_value, $client_sender_value, $client_recipient_value);

        return response()->json([
            'sender' => $sender,
            'recipient' => $recipient,
            'currency' => $currency,
            'value' => $value,
            'sender_currency' => $client_sender->currency,
            'sender_value' => $client_sender_value,
            'recipient_currency' => $client_recipient->currency,
            'recipient_value' => $client_recipient_value,
        ], 200);
    }
}
