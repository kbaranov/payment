<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Exports\ReportExport;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Client;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ClientController extends Controller
{
    /**
     * Create client.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (!in_array($request->post('currency'), Currency::$currency_list)) {
            return response()->json(['error' => 'Currency is not valid.'], 400);
        }

        $client = Client::create($request->all());

        return response()->json($client, 201);
    }

    /**
     * Returns report data.
     *
     * @param Request $request
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function report(Request $request, Client $client)
    {
        $date_from = $request->get('date_from') ?? null;
        $date_to = $request->get('date_to') ?? null;

        $transactions = Transaction::getReportData($client->id, $date_from, $date_to);

        $transactions_sum = Transaction::getReportSum($client->id, $date_from, $date_to);

        return view('client/report', [
            'client' => $client,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'transactions' => $transactions,
            'transactions_sum' => $transactions_sum['sum'],
            'transactions_base_sum' => $transactions_sum['base_sum'],
        ]);
    }

    /**
     * @param Request $request
     * @param Client $client
     *
     * @return Response|BinaryFileResponse
     */
    public function export(Request $request, Client $client)
    {
        $date_from = $request->get('date_from') ?? null;
        $date_to = $request->get('date_to') ?? null;

        return (new ReportExport($client->id, $date_from, $date_to))->download('report.xlsx');
    }
}
