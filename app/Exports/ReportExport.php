<?php

namespace App\Exports;

use App\Transaction;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Database\Eloquent\Builder as Eloquent_Builder;
use Illuminate\Database\Eloquent\Collection as Eloquent_Collection;
use Illuminate\Support\Collection as Support_Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExport implements FromCollection, WithHeadings
{
    use Exportable;

    private $client_id;
    private $date_from;
    private $date_to;

    /**
     * ReportExport constructor.
     *
     * @param int $client_id
     * @param string|null $date_from
     * @param string|null $date_to
     */
    public function __construct(int $client_id, string $date_from = null, string $date_to = null)
    {
        $this->client_id = $client_id;
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }

    /**
     * @return Eloquent_Builder[]|Eloquent_Collection|Support_Collection
     */
    public function collection()
    {
        return Transaction::getReportData($this->client_id, $this->date_from, $this->date_to);
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return ['Date', 'Currency', 'Value', 'Debit', 'Credit'];
    }
}
