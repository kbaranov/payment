<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Payment
 * @package App
 */
class Payment extends Model
{
    /**
     * Creating a transaction to charge the client's account.
     *
     * @param Client $client
     * @param string $currency
     * @param float $value
     * @param float $base_value
     * @param float $client_value
     *
     * @return Client
     */
    public static function charge(Client $client, string $currency, float $value, float $base_value, float $client_value)
    {
        $transaction = new Transaction();
        $transaction->currency = $currency;
        $transaction->value = $value;
        $transaction->base_value = $base_value;
        $transaction->client_id = $client->id;
        $transaction->client_debit = $client_value;

        $client->balance += $client_value;

        DB::transaction(function () use ($transaction, $client) {
            $transaction->save();
            $client->save();
        });

        return $client;
    }

    /**
     * Creating transactions to transfer money between client accounts.
     *
     * @param Client $client_sender
     * @param Client $client_recipient
     * @param string $currency
     * @param float $value
     * @param float $base_value
     * @param float $client_sender_value
     * @param float $client_recipient_value
     *
     * @return bool
     */
    public static function transfer(Client $client_sender, Client $client_recipient, string $currency, float $value,
                                    float $base_value, float $client_sender_value, float $client_recipient_value)
    {
        $hash = md5($client_sender->id . $client_recipient->id . $currency . $value . time() . str_random(8));

        $transaction_sender = new Transaction();
        $transaction_sender->currency = $currency;
        $transaction_sender->value = $value;
        $transaction_sender->base_value = $base_value;
        $transaction_sender->client_id = $client_sender->id;
        $transaction_sender->client_credit = $client_sender_value;
        $transaction_sender->hash = $hash;

        $transaction_recipient = new Transaction();
        $transaction_recipient->currency = $currency;
        $transaction_recipient->value = $value;
        $transaction_recipient->base_value = $base_value;
        $transaction_recipient->client_id = $client_recipient->id;
        $transaction_recipient->client_debit = $client_recipient_value;
        $transaction_recipient->hash = $hash;

        $client_sender->balance -= $client_sender_value;

        $client_recipient->balance += $client_recipient_value;

        DB::transaction(function () use ($transaction_sender, $transaction_recipient, $client_sender, $client_recipient) {
            $transaction_sender->save();
            $transaction_recipient->save();
            $client_sender->save();
            $client_recipient->save();
        });

        return true;
    }
}
