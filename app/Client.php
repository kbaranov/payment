<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * @package App
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $country
 * @property string $city
 * @property string $currency
 * @property float $balance
 *
 * @method static truncate
 * @method static create(array $data)
 * @method static Client find(int $client_id)
 */
class Client extends Model
{
    protected $fillable = ['name', 'country', 'city', 'currency'];

    protected $primaryKey = 'id';
}
