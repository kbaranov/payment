<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $date
 * @property string $currency
 * @property float $quote
 *
 * @method static truncate
 * @method static create(array $data)
 */
class Currency extends Model
{
    protected $fillable = ['date', 'currency', 'quote'];

    const BASE_CURRENCY = 'USD';

    /**
     * List of currency codes.
     *
     * @var array
     */
    public static $currency_list = ['AED', 'AFN', 'ALL', 'AMD', 'AOA', 'ARS', 'AUD', 'AZN', 'BDT', 'BGN', 'BHD', 'BIF',
        'BND', 'BOB', 'BRL', 'BWP', 'BYN', 'CAD', 'CDF', 'CHF', 'CLP', 'CNY', 'COP', 'CRC', 'CUP', 'CZK', 'DJF', 'DKK',
        'DZD', 'EGP', 'ETB', 'EUR', 'GBP', 'GEL', 'GHS', 'GMD', 'GNF', 'HKD', 'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'IQD',
        'IRR', 'ISK', 'JOD', 'JPY', 'KES', 'KGS', 'KHR', 'KPW', 'KRW', 'KWD', 'KZT', 'LAK', 'LBP', 'LKR', 'LYD', 'MAD',
        'MDL', 'MGA', 'MKD', 'MNT', 'MRO', 'MUR', 'MWK', 'MXN', 'MYR', 'MZN', 'NAD', 'NGN', 'NIO', 'NOK', 'NPR', 'NZD',
        'OMR', 'PEN', 'PHP', 'PKR', 'PLN', 'PYG', 'QAR', 'RON', 'RSD', 'RUB', 'SAR', 'SCR', 'SDG', 'SEK', 'SGD', 'SLL',
        'SOS', 'SRD', 'SYP', 'SZL', 'THB', 'TJS', 'TMT', 'TND', 'TRY', 'TWD', 'TZS', 'UAH', 'UGX', 'USD', 'UYU', 'UZS',
        'VEF', 'VND', 'XAF', 'XDR', 'XOF', 'YER', 'ZAR', 'ZMK'];

    /**
     * Returns currency quote by currency code for certain date.
     *
     * @param string $currency
     * @param string|null $date
     *
     * @return float
     */
    public static function getQuote(string $currency, string $date = null)
    {
        $date = date('Y-m-d', !is_null($date) ? strtotime($date) : time());

        return Currency::query()
            ->where('currency', $currency)
            ->where('date', $date)
            ->first()->quote;
    }

    /**
     * Checks currency for validness.
     *
     * @param string $currency
     *
     * @return bool
     */
    public static function check(string $currency)
    {
        return in_array($currency, self::$currency_list);
    }

    /**
     * Converts value from currency to base currency.
     *
     * @param string $currency
     * @param float $value
     *
     * @return float|false
     */
    public static function convertToBase(string $currency, float $value)
    {
        if (!self::check($currency)) {
            return false;
        }

        if ($currency == self::BASE_CURRENCY) {
            return $value;
        }

        $converted_value = $value / Currency::getQuote($currency);

        return round($converted_value, 2);
    }

    /**
     * Converts base value from base currency to client account currency.
     *
     * @param string $to_currency
     * @param float $base_value
     *
     * @return float|false
     */
    public static function convertFromBase(string $to_currency, float $base_value)
    {
        if (!self::check($to_currency)) {
            return false;
        }

        if ($to_currency == self::BASE_CURRENCY) {
            return $base_value;
        }

        $converted_value = $base_value * Currency::getQuote($to_currency);

        return round($converted_value, 2);
    }
}
