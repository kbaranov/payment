<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Transaction
 * @package App
 *
 * @property int $id
 * @property string $created_at
 * @property string $currency
 * @property float $value
 * @property float $base_value
 * @property int $client_id
 * @property float $client_debit
 * @property float $client_credit
 * @property string $hash
 *
 * @method static truncate
 * @method static create(array $data)
 */
class Transaction extends Model
{
    protected $fillable = ['currency', 'value'];

    public $updated_at = false;

    /**
     * @param int $client_id
     * @param string|null $date_from
     * @param string|null $date_to
     *
     * @return Builder
     */
    private static function getReportQuery(int $client_id, string $date_from = null, string $date_to = null)
    {
        $transactions = (new Transaction())->newQuery()
            ->where('client_id', $client_id)
            ->orderBy('created_at');

        if (!empty($date_from)) {
            $transactions->where('created_at', '>=', $date_from);
        }

        if (!empty($date_to)) {
            $transactions->where('created_at', '<=', $date_to);
        }

        return $transactions;
    }

    /**
     * @param int $client_id
     * @param string|null $date_from
     * @param string|null $date_to
     *
     * @return Builder[]|Collection
     */
    public static function getReportData(int $client_id, string $date_from = null, string $date_to = null)
    {
        $transactions = self::getReportQuery($client_id, $date_from, $date_to);

        $transactions->select(['created_at', 'currency', 'value', 'client_debit', 'client_credit']);

        return $transactions->get();
    }

    /**
     * @param int $client_id
     * @param string|null $date_from
     * @param string|null $date_to
     *
     * @return Builder[]|Collection
     */
    public static function getReportSum(int $client_id, string $date_from = null, string $date_to = null)
    {
        $transactions = self::getReportQuery($client_id, $date_from, $date_to);

        $transactions->selectRaw('sum(base_value) as base_sum, sum(client_debit + client_credit) as sum');

        return $transactions->first()->toArray();
    }
}
