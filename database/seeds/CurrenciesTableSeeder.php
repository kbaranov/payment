<?php

use Illuminate\Database\Seeder;
use App\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::truncate();

        foreach (Currency::$currency_list as $currency_code) {

            if ($currency_code == Currency::BASE_CURRENCY) {
                continue;
            }

            $number = rand(5000, rand(10000, rand(50000, rand(100000, rand(500000, 999999)))));
            $number = rand(1, rand(5, rand(10, rand(50, rand(100, rand(500, rand(1000, $number)))))));
            $decimals = rand(1, 9999);
            $quote = $number * $decimals / 10000;

            for ($i = 30; $i >= 0; $i--) {

                $currency = new Currency();
                $currency->date = date('Y-m-d', (time() - 3600 * 24 * $i));
                $currency->currency = $currency_code;
                $currency->quote = $quote;
                $currency->save();

                $quote_new = $quote * (1 + (rand(0, 1) ? 1 : -1) * (rand(1, 10) / 1000));
                if ($quote_new <= 0) {
                    continue;
                }
                $quote = $quote_new;
            }
        }
    }
}
