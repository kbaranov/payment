<?php

use Illuminate\Database\Seeder;
use App\Transaction;
use App\Client;
use App\Currency;
use App\Payment;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::truncate();

        $clients = Client::all();

        foreach ($clients as $client_sender) {

            $added_value = rand(100, 1000);

            if ($client_sender->currency != Currency::BASE_CURRENCY) {
                $added_value *= (int) Currency::getQuote($client_sender->currency);
            }

            $base_value = Currency::convertToBase($client_sender->currency, $added_value);

            $client_sender = Payment::charge($client_sender, $client_sender->currency, $added_value, $base_value, $added_value);

            foreach ($clients as $client_recipient) {

                if ($client_sender->id == $client_recipient->id) {
                    continue;
                }

                $currency = [$client_sender->currency, $client_recipient->currency][rand(0, 1)];
                $value = rand(1, 10);

                $base_value = Currency::convertToBase($currency, $value);

                if ($client_sender->currency == $currency) {
                    $client_sender_value = $value;
                } else {
                    $client_sender_value = Currency::convertFromBase($client_sender->currency, $base_value);
                }

                if ($client_sender_value > $client_sender->balance) {
                    continue;
                }

                if ($client_recipient->currency == $currency) {
                    $client_recipient_value = $value;
                } else {
                    $client_recipient_value = Currency::convertFromBase($client_recipient->currency, $base_value);
                }

                Payment::transfer($client_sender, $client_recipient, $currency, $value, $base_value, $client_sender_value, $client_recipient_value);
            }
        }
    }
}
