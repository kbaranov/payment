<?php

use Illuminate\Database\Seeder;
use App\Client;
use App\Currency;
use Faker\Factory;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::truncate();

        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $client = new Client();
            $client->name = $faker->name;
            $client->country = $faker->country;
            $client->city = $faker->city;
            $client->currency = Currency::$currency_list[array_rand(Currency::$currency_list)];
            $client->save();
        }
    }
}
