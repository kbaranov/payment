<?php

use App\Client;
use App\Currency;
use App\Transaction;

/**
 * @var Client $client
 * @var string $date_from
 * @var string $date_to
 * @var Transaction[] $transactions
 * @var float $transactions_sum
 * @var float $transactions_base_sum
 */
?>

@extends('layouts.app')

@section('title', 'Client Account Report')

@section('content')

    <h2>Client Account Report</h2>
    <h4>{{ $client->name }}, {{ $client->city }}, {{ $client->country }}</h4>
    <h5>Account currency: {{ $client->currency }}</h5>

    <br />
    <div class="row">
        <div class="col-10">
            @if(!empty($date_from) && !empty($date_to))
                <h5>Period: {{ date('d.m.Y', strtotime($date_from)) }} &ndash; {{ date('d.m.Y', strtotime($date_to)) }}</h5>
            @elseif(!empty($date_from))
                <h5>Period: from {{ date('d.m.Y', strtotime($date_from)) }}</h5>
            @elseif(!empty($date_to))
                <h5>Period: till {{ date('d.m.Y', strtotime($date_to)) }}</h5>
            @endif
            <h5>Total amount: {{ $client->currency }} {{ $transactions_sum }} (<?php echo Currency::BASE_CURRENCY ?> {{ $transactions_base_sum }})</h5>
        </div>
        <div class="col-2 text-right"><button type="button" class="btn btn-secondary" onclick="(function(){location.href='{{ str_replace('report', 'export', $_SERVER['REQUEST_URI']) }}'})();">Export to XLS</button></div>
    </div>

    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Transaction</th>
            <th scope="col">Debit</th>
            <th scope="col">Credit</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($transactions as $transaction)
            <tr>
                <td>{{ date('d.m.Y H:i', strtotime($transaction->created_at)) }}</td>
                <td>{{ $transaction->currency }} {{ number_format($transaction->value, 2) }}</td>
                <td>{{ number_format($transaction->client_debit, 2) }}</td>
                <td>{{ number_format($transaction->client_credit, 2) }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection