<?php

namespace Tests\Feature;

use App\Client;
use App\Currency;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory;

class TransferTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $faker = Factory::create();

        Currency::truncate();

        $currency = new Currency();
        $currency->date = date('Y-m-d');
        $currency->currency = 'RUB';
        $currency->quote = 40;
        $currency->save();

        $currency = new Currency();
        $currency->date = date('Y-m-d');
        $currency->currency = 'EUR';
        $currency->quote = 0.5;
        $currency->save();

        $client_sender = new Client();
        $client_sender->name = $faker->name;
        $client_sender->country = $faker->country;
        $client_sender->city = $faker->city;
        $client_sender->currency = 'RUB';
        $client_sender->balance = 0;
        $client_sender->save();

        $client_recipient = new Client();
        $client_recipient->name = $faker->name;
        $client_recipient->country = $faker->country;
        $client_recipient->city = $faker->city;
        $client_recipient->currency = 'EUR';
        $client_recipient->balance = 0;
        $client_recipient->save();

        $this->assertEquals(0, Client::find($client_sender->id)->balance);
        $this->assertEquals(0, Client::find($client_recipient->id)->balance);

        $this->post('api/charge', [
            "client" => $client_sender->id,
            "currency" => "USD",
            "value" => 125,
        ])->assertStatus(200);
        $this->assertEquals(5000, Client::find($client_sender->id)->balance);
        $this->assertEquals(0, Client::find($client_recipient->id)->balance);

        $this->post('api/charge', [
            "client" => $client_sender->id,
            "currency" => "RUB",
            "value" => 5000,
        ])->assertStatus(200);
        $this->assertEquals(10000, Client::find($client_sender->id)->balance);
        $this->assertEquals(0, Client::find($client_recipient->id)->balance);

        $this->post('api/transfer', [
            "sender" => $client_sender->id,
            "recipient" => $client_recipient->id,
            "currency" => "RUB",
            "value" => 4000,
        ])->assertStatus(200);
        $this->assertEquals(6000, Client::find($client_sender->id)->balance);
        $this->assertEquals(50, Client::find($client_recipient->id)->balance);

        $this->post('api/transfer', [
            "sender" => $client_sender->id,
            "recipient" => $client_recipient->id,
            "currency" => "EUR",
            "value" => 50,
        ])->assertStatus(200);
        $this->assertEquals(2000, Client::find($client_sender->id)->balance);
        $this->assertEquals(100, Client::find($client_recipient->id)->balance);

        $this->post('api/transfer', [
            "sender" => $client_recipient->id,
            "recipient" => $client_sender->id,
            "currency" => "RUB",
            "value" => 8000,
        ])->assertStatus(200);
        $this->assertEquals(10000, Client::find($client_sender->id)->balance);
        $this->assertEquals(0, Client::find($client_recipient->id)->balance);
    }
}
