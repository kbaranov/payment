<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUserSuccess()
    {
        $data = [
            "name" => "Ivanov Ivan",
            "country" => "Russia",
            "city" => "Moscow",
            "currency" => "RUB",
        ];

        $structure = [
            "id",
            "name",
            "country",
            "city",
            "currency",
            "updated_at",
            "created_at",
        ];

        $this->post('api/client', $data)
            ->assertStatus(201)
            ->assertJsonStructure($structure);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUserFailure()
    {
        $data_set = [
            [
                "name" => "Ivanov Ivan",
                "country" => "Russia",
                "city" => "Moscow",
                "currency" => "XXX",
            ],
            [
                "name" => "Ivanov Ivan",
                "country" => "Russia",
                "city" => "Moscow",
            ]
        ];

        $structure = [
            "error",
        ];

        foreach ($data_set as $data) {
            $this->post('api/client', $data)
                ->assertStatus(400)
                ->assertJsonStructure($structure);
        }
    }
}
